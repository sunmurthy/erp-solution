package com.enquiry.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.enquiry.entity.RegisterForm;
import com.enquiry.services.EnquiryService;

@RestController
public class EnquiryController {

	
	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	private EnquiryService service;
	
	@PostMapping("/register")
	public String register(@RequestBody @Valid RegisterForm form) {
		form.setPassWord(encoder.encode(form.getPassWord()));
		return service.register(form);
	}
	
	
	
	@GetMapping("/users")
	public List<RegisterForm> users() {
		return service.fetchUsers();
	}
	

	
	@ResponseStatus(value=HttpStatus.NOT_ACCEPTABLE,
            reason="some fields should not be empty")  // 409
@ExceptionHandler(MethodArgumentNotValidException.class)
public void conflict() {
// Nothing to do
}
	
}
