package com.enquiry.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enquiry.dao.EnquiryDao;
import com.enquiry.entity.RegisterForm;

@Service
public class EnquiryService {

	@Autowired
	private EnquiryDao dao;

	public String register(RegisterForm form) {

		dao.register(form);
		return "registerterd successfully";
	}

	public List<RegisterForm> fetchUsers() {
		// TODO Auto-generated method stub
		List<RegisterForm> forms = dao.fetchUsers();
		forms.forEach(r -> r.setPassWord(null));
		return forms;
	}

}
