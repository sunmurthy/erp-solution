package com.enquiry.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.enquiry.entity.RegisterForm;
import com.enquiry.repository.EnquiryRepo;

@Component
public class EnquiryDao {
	
	
	@Autowired
	private EnquiryRepo repo;

	public void register(RegisterForm form) {
		  repo.save(form);
		
	}

	public List<RegisterForm> fetchUsers() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
