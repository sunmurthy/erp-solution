package com.enquiry.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.enquiry.entity.RegisterForm;

public interface EnquiryRepo extends MongoRepository<RegisterForm, String> {

}
