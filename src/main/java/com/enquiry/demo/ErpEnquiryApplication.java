package com.enquiry.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan("com.enquiry.*")
@EnableMongoRepositories("com.enquiry.repository")
//@EnableAutoConfiguration
public class ErpEnquiryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErpEnquiryApplication.class, args);
	}
}
